/*
* PermanentRelay.cpp 
* Version 1.0 May, 2015
* Copyright 2015 Pablo Rodiz Obaya
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
* The latest version of this library can always be found at
* https://bitbucket.org/enxine/permanentrelay
* 
*/

#include "PermanentRelay.h"

#define _DEBUG false

PermanentRelay::PermanentRelay(unsigned int uiPin, unsigned int uiEEPROMPosition) : EEPROMPosition(uiEEPROMPosition), pin(uiPin)
{
  if (_DEBUG) {Serial.print("Setting up relay control");};
  pinMode(pin, OUTPUT);
  if(this->switchEnabled = loadRelayFromEEPROM()) this->setSwitchON();
}

void PermanentRelay::saveRelayToEEPROM(boolean val) {
  char rel;
  if (_DEBUG) {
    Serial.print("saveRelayToEEPROM(");
    if(val) Serial.println("true)");
    else Serial.println("false)");
  };
  if(val) rel = 0x55;
  else rel = 0x00;
  
  if (EEPROM.read(EEPROMPosition) != rel) EEPROM.write(EEPROMPosition, rel);
    // and verifies the data
  if (EEPROM.read(EEPROMPosition) != rel) {
    if (_DEBUG) {Serial.println("Error writing to EEPROM");};
  }  
}

boolean PermanentRelay::loadRelayFromEEPROM() {
  if (_DEBUG) {Serial.print("loadRelayFromEEPROM()");};
  char rel = EEPROM.read(EEPROMPosition);
  if(rel == 0x55){
    if (_DEBUG) {Serial.println("=true");};
    return(true);
  }  
  else {
    if (_DEBUG) {Serial.println("=false");};
    return(false);
  }  
}

void PermanentRelay::setSwitchON() {
  if (_DEBUG) {Serial.print("setSwitchON()");};
  if(!this->switchEnabled) {
    saveRelayToEEPROM(true);
  }  
  this->switchEnabled = true;
  digitalWrite(this->pin, HIGH);
}

void PermanentRelay::setSwitchOFF() {
  if (_DEBUG) {Serial.print("setSwitchOFF()");};
  if(this->switchEnabled) {
    saveRelayToEEPROM(false);
  }  
  this->switchEnabled = false;
  digitalWrite(this->pin, LOW);
}

boolean PermanentRelay::getSwitchState() {
  return(this->switchEnabled);
}  

