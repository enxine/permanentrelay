/*
* PermanentRelay.h 
* Version 1.0 May, 2015
* Copyright 2015 Pablo Rodiz Obaya
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
* The latest version of this library can always be found at
* https://bitbucket.org/enxine/permanentrelay
* 
*/

#ifndef _PermanentRelay_h
#define _PermanentRelay_h

#include <Arduino.h>
#include <EEPROM.h>

class PermanentRelay {

  public:
    PermanentRelay(unsigned int pin, unsigned int EEPROMPosition);
    void setSwitchON();
    void setSwitchOFF();
    boolean getSwitchState();
    
  private:
    bool switchEnabled;
    unsigned int pin;
    unsigned int EEPROMPosition;
    boolean loadRelayFromEEPROM();
    void saveRelayToEEPROM(boolean val);

};
#endif //_PermanentRelay_h
