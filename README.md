# PermanentRelay Arduino Library
This library enables you to easily define relays connected to Arduino digital pins, control
them, and store their state in the Arduino's internal non-volatile memory (EEPROM). This way
when a power cycle happens, the relays, after complete reboot, keep tha same state than before
the power was removed from the board. 

## Version - 1.00

## Installation

1. Download the code and store it in a folder called PermanentRelay in your libraries directory. 
2. Restart Arduino IDE and look for PermanentRelay in the libraries list. 

## Usage

The library is very easy to use. Once installed in your Arduino development environment you
just need to create an instance of the PermanentRelay object for every relay you have in your
circuit:

```PermanentRelay Relay1 = PermanentRelay(PIN_RELAY, EEPROM_START)```

Constructor only takes two parameters, the pin number where the relay control circuit is
connected and the starting address of the EEPROM where you want to store its state. The 
microcontrollers on the various Arduino boards have different amounts of EEPROM: 1024 bytes
on the ATmega328, 512 bytes on the ATmega168 and ATmega8, 4 KB (4096 bytes) on the ATmega1280
and ATmega2560. this is a byte address, so you can use numbers from 0 to 511 on 512 bytes 
EEPROMs and so on. If you intend to use multiple relays make sure you use different address
numbers for each relay, or all will share the same state after power on.

Once you have the PermanentRelay object created you can turn the relay on:

```Relay1.setSwitchON()```

Turn it off:

```Relay1.setSwitchOFF()```
 
Or just check current state:

```boolean state = Relay1.getSwitchState();```
 
## Contributing
If you want to contribute to this project:
- Report bugs and errors
- Ask for enhancements
- Create issues and pull requests
- Tell other people about this library

## Copyright
Copyright 2015 Pablo Rodiz Obaya