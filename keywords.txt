#######################################
# Syntax Coloring Map For PermanentRelay
#######################################

#######################################
# Class name (KEYWORD1)
#######################################

PermanentRelay	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################

setSwitchON	KEYWORD2
setSwitchOFF	KEYWORD2
getSwitchState	KEYWORD2

