/*
* oneRelay.ino 
* Version 1.0 May, 2015
* Copyright 2015 Pablo Rodiz Obaya
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/

//Do not forget to include EEPROM.h. This library is used inside the PermanentRelay library,
//And inclided from there, but I did not find the way to avoid the need of including it again 
//from the sketch
#include <EEPROM.h>
//Include permanent relay library
#include <PermanentRelay.h>

//I have my relay connected to digital pin 8
#define RELAY_PIN 8
//And I want the state to be stored in the first byte of the EEPROM
#define EEPROM_ADDRESS 0

//Create the objetc to control the relay
PermanentRelay myRelay = PermanentRelay(RELAY_PIN, EEPROM_ADDRESS);

void setup() {  
  Serial.begin(9600);
  while (!Serial); // wait for serial port to connect. Needed for Leonardo only
  Serial.println("PermanentRelay library example oneRelay");
  Serial.println("1 - Switch relay ON");
  Serial.println("0 - Switch relay OFF");
  Serial.println("Try turning the relay on, power off the board,");
  Serial.println("and see how it will turn on automatically when");
  Serial.println("you power on the board again.");
}

void loop() {
   char c;
   //Read charcters from serial interface
   if(Serial.available()) {
     c = Serial.read();
	 Serial.println(c);
   }
   //If 1 received turn relay on
   if (c=='1') {
     Serial.println("Switching relay ON");
     myRelay.setSwitchON();
   }	 
   //If 0 received turn relay OFF
   if (c=='0') {
     Serial.println("Switching relay OFF");
     myRelay.setSwitchOFF();
  }	 
}   