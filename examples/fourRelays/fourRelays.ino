/*
* fourRelays.ino 
* Version 1.0 May, 2015
* Copyright 2015 Pablo Rodiz Obaya
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/

//Do not forget to include EEPROM.h. This library is used inside the PermanentRelay library,
//And inclided from there, but I did not find the way to avoid the need of including it again 
//from the sketch
#include <EEPROM.h>
//Include permanent relay library
#include <PermanentRelay.h>

//For this example I used Seeed Studio Relay Board V2.0 http://www.seeedstudio.com/wiki/Relay_Shield_V2.0
//which has four relays connected digital pins 4, 5, 6 and 7
#define RELAY1_PIN 7
#define RELAY2_PIN 6
#define RELAY3_PIN 5
#define RELAY4_PIN 4
//And I want the state to be stored in the first four bytes of the EEPROM
#define EEPROM_ADDRESS1 0
#define EEPROM_ADDRESS2 1
#define EEPROM_ADDRESS3 2
#define EEPROM_ADDRESS4 3

//Create the objetcs to control the relays
PermanentRelay myRelay1 = PermanentRelay(RELAY1_PIN, EEPROM_ADDRESS1);
PermanentRelay myRelay2 = PermanentRelay(RELAY2_PIN, EEPROM_ADDRESS2);
PermanentRelay myRelay3 = PermanentRelay(RELAY3_PIN, EEPROM_ADDRESS3);
PermanentRelay myRelay4 = PermanentRelay(RELAY4_PIN, EEPROM_ADDRESS4);

void setup() {  
  Serial.begin(9600);
  while (!Serial); // wait for serial port to connect. Needed for Leonardo only
  Serial.println("PermanentRelay library example fourRelays");
  Serial.println("1 - Change state of relay 1");
  Serial.println("2 - Change state of relay 2");
  Serial.println("3 - Change state of relay 3");
  Serial.println("4 - Change state of relay 4");
  Serial.println("Try turning one relay on, power off the board,");
  Serial.println("and see how it will turn on automatically when");
  Serial.println("you power on the board again.");
}

void loop() {
   char c;
   //Read charcters from serial interface
   if(Serial.available()) {
     c = Serial.read();
	 Serial.println(c);
   }
   //If 1 received change state of relay 1
   if (c=='1') {
     if(myRelay1.getSwitchState()) {
       Serial.println("Switching relay 1 OFF");
       myRelay1.setSwitchOFF();
	 } else {
       Serial.println("Switching relay 1 ON");
       myRelay1.setSwitchON();
	 }  
   }	 
   //If 2 received change state of relay 2
   if (c=='2') {
     if(myRelay2.getSwitchState()) {
       Serial.println("Switching relay 2 OFF");
       myRelay2.setSwitchOFF();
	 } else {
       Serial.println("Switching relay 2 ON");
       myRelay2.setSwitchON();
	 }  
   }	 
   //If 1 received change state of relay 1
   if (c=='3') {
     if(myRelay3.getSwitchState()) {
       Serial.println("Switching relay 3 OFF");
       myRelay3.setSwitchOFF();
	 } else {
       Serial.println("Switching relay 3 ON");
       myRelay3.setSwitchON();
	 }  
   }	 
   //If 1 received change state of relay 4
   if (c=='4') {
     if(myRelay4.getSwitchState()) {
       Serial.println("Switching relay 4 OFF");
       myRelay4.setSwitchOFF();
	 } else {
       Serial.println("Switching relay 4 ON");
       myRelay4.setSwitchON();
	 }  
   }	 
}   